package example.contoh.exampleview;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import example.contoh.exampleview.widget.InflateLayout;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.tariff)
    InflateLayout tarif;
    Context context;
    @BindView(R.id.push)
    Button push;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        context = this;
        tarif.hide(true);

    }

    @OnClick(R.id.push) void onClick() {
        Toast.makeText(context, "Berhasil Keluar", Toast.LENGTH_LONG).show();
        tarif.show();

    }
}
