package example.contoh.exampleview.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import example.contoh.exampleview.R;

/**
 * Created by feetbo on 07/07/17.
 **/

public class InflateLayout extends FrameLayout implements CompoundButton.OnCheckedChangeListener{
    @BindView(R.id.tariffviewJarak)
    TextView jarak;
    @BindView(R.id.tariffviewOldPrice)
    TextView Hargalama;
    @BindView(R.id.tariffviewNormalPrice)
    TextView HargaNormal;
    @BindView(R.id.tariffviewLowPrice)
    TextView HargaMinim;
    @BindView(R.id.tariffviewReload)
    TextView reload;
    @BindView(R.id.sbRadioButton1)
    RadioButton rb1;
    @BindView(R.id.sbRadioButton2)
    RadioButton rb2;
    @BindView(R.id.tariffviewLinearLayout1)
    View jarakbar;
    @BindView(R.id.tariffviewLinearLayout2)
    View ParentView;
    @BindView(R.id.pesan)
    Button pesan;

    Context context;

    View v;

    public InflateLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView();
    }

    private void initView() {

        v= LayoutInflater.from(getContext()).inflate(R.layout.inflate_view, null);
        ButterKnife.bind(this, v);

        //pesan.setOnClickListener(this);

        addView(v);
    }




    public InflateLayout(Context context) {
        super(context);
        this.context = context;

        initView();
    }
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

    }

    public void setJarak(String s){
        jarak.setText(s);
    }

    public void hide(final boolean animate){
        ParentView.post(new Runnable(){
            @Override
            public void run() {
                animate().setStartDelay(0);
                animate().setDuration(animate?500:0);
                animate().translationY(ParentView.getHeight());
                animate().start();
            }
        });
    }


    public void show(){
        animate().setStartDelay(1000);
        animate().setDuration(1500);
        animate().translationY(0);
        animate().start();
    }
    public void setTarifByJarak(double d){
        long tarif=0;
        long tarifdisc=0;
        if(d<=3){
            tarif=6000;
            tarifdisc=tarif-1000;
        }else{
            tarif=Math.round(((d-3)*2000)+6000);
            tarifdisc=tarif-(tarif>20000?10000:1000);
        }
        long roundedTarif = ((tarif + 99) / 100 ) * 100;
        long roundedTarifdisc = ((tarifdisc + 99) / 100 ) * 100;
        HargaNormal.setText(priceFormater(roundedTarif, "Rp"));
        Hargalama.setText(priceFormater(roundedTarif, "Rp"));
        HargaMinim.setText(priceFormater(roundedTarifdisc, "Rp"));
    }
    public String priceFormater(long s, String currency){
        return (currency+s).replaceAll("(\\d)(?=(\\d{3})+(?!\\d))", "$1.");
    }

    /* Tidak perlu digunakan lagi karena sudah menggunakan butterknife
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pesan:
                Toast.makeText(context, "Berhasil", Toast.LENGTH_LONG).show();
                hide(true);
                break;
            default:
                break;
        }
    }*/

    @OnClick(R.id.pesan) void onClick() {
        Toast.makeText(context, "Berhasil Hilang", Toast.LENGTH_LONG).show();
        hide(true);

    }
}
